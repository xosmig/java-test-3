import com.xosmig.tictactoe.TicTacToeApp
import javafx.application.Application

fun main(args: Array<String>) {
    Application.launch(TicTacToeApp::class.java, *args)
}
