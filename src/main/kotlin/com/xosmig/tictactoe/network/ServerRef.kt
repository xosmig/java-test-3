package com.xosmig.tictactoe.network

import com.xosmig.tictactoe.Game
import com.xosmig.tictactoe.utils.Point
import java.io.Closeable
import kotlin.reflect.KClass

interface ServerRef: Closeable {

    fun makeTurn(point: Point)

    fun<T: Game.TurnResult> addCallback(clazz: KClass<T>, callback: (T) -> Unit)
}

