package com.xosmig.tictactoe.network

import com.xosmig.tictactoe.Game
import com.xosmig.tictactoe.utils.Point
import java.net.InetSocketAddress
import kotlin.reflect.KClass

class Client(private val address: InetSocketAddress): ServerRef {

    override fun makeTurn(point: Point) {
        // TODO
    }

    override fun <T : Game.TurnResult> addCallback(clazz: KClass<T>, callback: (T) -> Unit) {
        // TODO
    }

    override fun close() {
        // TODO
    }
}
