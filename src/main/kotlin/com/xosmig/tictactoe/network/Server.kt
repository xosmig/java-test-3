package com.xosmig.tictactoe.network

import com.xosmig.tictactoe.Game
import com.xosmig.tictactoe.utils.Point
import java.net.ServerSocket
import kotlin.reflect.KClass

class Server(port: Int): Runnable, ServerRef {

    private val game = Game()
    private val socket = ServerSocket(port)
    private val callbacks = HashMap<KClass<out Game.TurnResult>, (Game.TurnResult) -> Unit>()

    override fun run() {
        throw UnsupportedOperationException("not implemented")
    }

    override fun makeTurn(point: Point) {
        val turnRes = game.makeTurn(point, Game.Player.FIRST)
    }

    override fun <T: Game.TurnResult> addCallback(clazz: KClass<T>, callback: (T) -> Unit) {
//        callbacks[clazz] = callback
        // TODO
    }

    override fun close() {
        // TODO
    }
}
