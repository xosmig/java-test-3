package com.xosmig.tictactoe.utils

class Grid<T: Any>(width: Int, height: Int, default: () -> T) {
    private val underline = ArrayList<ArrayList<T>>()

    init {
        for (i in 1..height) {
            val newLine = ArrayList<T>()
            for (j in 1..width) {
                newLine.add(default())
            }
            underline.add(newLine)
        }
    }

    val size get() = underline.size * underline[0].size
    val lines get(): List<List<T>> = underline

    operator fun set(point: Point, newVal: T) { underline[point.lineN][point.columnN] = newVal }
    operator fun get(point: Point) = underline[point.lineN][point.columnN]
    operator fun get(lineN: Int) = underline[lineN]
}
