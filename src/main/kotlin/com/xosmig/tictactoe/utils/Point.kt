package com.xosmig.tictactoe.utils

data class Point internal constructor(val lineN: Int, val columnN: Int)
