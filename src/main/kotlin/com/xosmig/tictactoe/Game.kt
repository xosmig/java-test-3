package com.xosmig.tictactoe

import com.xosmig.tictactoe.utils.Grid
import com.xosmig.tictactoe.utils.Point

class Game {

    companion object {
        internal const val EMPTY: Sign = ' '
        val SIZE: Int = 3
    }

    private val board = Grid<Sign>(SIZE, SIZE) { EMPTY }
    private var currentPlayer = Player.FIRST
    private var freeCellsNum = board.size

    fun makeTurn(point: Point, player: Player): List<TurnResult> {
        val result = ArrayList<TurnResult>()

        if (player != currentPlayer) {
            return result
        }

        board[point] = player.sign
        freeCellsNum -= 1
        result.add(TurnResult.UpdateCell(point, player.sign))

        var winV = true
        var winH = true
        var winD1 = true
        var winD2 = true
        for (i in 0..(SIZE - 1)) {
            if (board[point.lineN][i] != player.sign) { winH = false }
            if (board[i][point.columnN] != player.sign) { winV = false }
            if (board[i][i] != player.sign) { winD1 = false }
            if (board[i][SIZE - i - 1] != player.sign) { winD2 = false }
        }
        if (winV || winH || winD1 || winD2) {
            result.add(TurnResult.Winner(winner = player))
        } else if (freeCellsNum == 0) {
            result.add(TurnResult.Draw())
        }

        return result
    }



    enum class Player(val sign: Sign) {
        FIRST('X'),
        SECOND('O')
    }

    open class TurnResult private constructor() {
        class UpdateCell(val point: Point, val newValue: Sign): TurnResult()
        class Winner(val winner: Player): TurnResult()
        class Draw: TurnResult()
    }
}
