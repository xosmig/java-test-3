package com.xosmig.tictactoe

import com.xosmig.tictactoe.network.Client
import com.xosmig.tictactoe.network.Server
import com.xosmig.tictactoe.ui.GameScene
import com.xosmig.tictactoe.ui.MainMenuScene
import com.xosmig.tictactoe.ui.MainMenuSceneListener
import com.xosmig.tictactoe.ui.SceneManager
import javafx.application.Application
import javafx.stage.Stage
import java.net.InetSocketAddress

class TicTacToeApp: Application() {

    companion object {
        private const val TITLE: String = "Tic-Tac-Toe"
        private const val PORT: Int = 8173
    }

    override fun start(primaryStage: Stage) {
        val sceneManager = SceneManager(primaryStage)
        primaryStage.title = TITLE

        val mainMenuScene = MainMenuScene()
        mainMenuScene.listener = object: MainMenuSceneListener {
            override fun hostButton() {
                sceneManager.show(GameScene(mainMenuScene, Server(InetSocketAddress(PORT)), sceneManager))
            }

            override fun connectButton() {
                sceneManager.show(GameScene(mainMenuScene, Client(InetSocketAddress("localhost", PORT)), sceneManager))
            }

            override fun exitButton() {
                sceneManager.disposeOne()
            }
        }

        sceneManager.show(mainMenuScene)
        primaryStage.show()
    }
}

