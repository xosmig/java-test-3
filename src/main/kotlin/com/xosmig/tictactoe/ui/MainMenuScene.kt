package com.xosmig.tictactoe.ui

import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.layout.VBox

class MainMenuScene private constructor( private val parent: VBox,
                                         width: Double,
                                         height: Double ): MyScene(parent, width, height) {

    constructor(width: Double = 200.0, height: Double = 200.0)
            : this(VBox(2.0), width, height)

    private val buttons = ArrayList<Button>()
    private val hostButton = addButton("Become a host")
    private val connectButton = addButton("Connect")
    private val exitButton = addButton("Exit")
    var listener: MainMenuSceneListener = MainMenuSceneListener.EmptyListener

    fun addButton(text: String) = Button(text).apply { buttons.add(this) }

    init {
        for (button in buttons) {
            button.prefWidthProperty().bind(this.widthProperty())
            button.prefHeightProperty().bind(this.heightProperty().divide(buttons.size))
            parent.children.add(button)
        }

        hostButton.setOnMouseClicked { listener.hostButton() }
        connectButton.setOnMouseClicked { listener.connectButton() }
        exitButton.setOnMouseClicked { listener.exitButton() }
    }
}

interface MainMenuSceneListener {
    fun hostButton(): Unit
    fun connectButton(): Unit
    fun exitButton(): Unit

    object EmptyListener: MainMenuSceneListener {
        override fun hostButton() {}
        override fun connectButton() {}
        override fun exitButton() {}
    }
}
