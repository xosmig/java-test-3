package com.xosmig.tictactoe.ui

import javafx.application.Platform
import javafx.scene.Scene
import javafx.stage.Stage
import java.io.Closeable
import java.util.*

class SceneManager(var stage: Stage): Closeable {

    private val scenes = Stack<MyScene>()

    fun show(scene: MyScene) {
        scenes.add(scene)
        setScene(scene)
    }

    fun disposeOne() {
        scenes.pop().close()
        if (scenes.empty()) {
            Platform.exit()
            return
        }
        setScene(scenes.peek())
    }

    private fun setScene(scene: Scene) { stage.scene = scene }

    override fun close() {
        for (scene in scenes) {
            scene.close()
        }
    }
}

