package com.xosmig.tictactoe.ui

import com.xosmig.tictactoe.Game
import com.xosmig.tictactoe.network.ServerRef
import com.xosmig.tictactoe.utils.Grid
import com.xosmig.tictactoe.utils.Point
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox

class GameScene private constructor( private val vbox: VBox,
                                     width: Double,
                                     height: Double,
                                     private val serverRef: ServerRef,
                                     private val sceneManager: SceneManager ): MyScene(vbox, width, height) {

    constructor(width: Double, height: Double, serverRef: ServerRef, sceneManager: SceneManager):
            this(VBox(), width, height, serverRef, sceneManager)
    constructor(previous: Scene, serverRef: ServerRef, sceneManager: SceneManager):
            this(previous.width, previous.height, serverRef, sceneManager)

    private val buttons = Grid(Game.SIZE, Game.SIZE) { Button() }
    private val menu = HBox().apply {
        val exitButton = Button("Exit")
        exitButton.setOnMouseClicked { sceneManager.disposeOne() }
        children.add(exitButton)

        exitButton.prefWidthProperty().bind(widthProperty().divide(children.size))
        exitButton.prefHeightProperty().bind(heightProperty())

        vbox.children.add(this)
    }
    private val grid = GridPane().apply {
        prefWidthProperty().bind(vbox.widthProperty())
        prefHeightProperty().bind(vbox.heightProperty().subtract(menu.heightProperty()))
        vbox.children.add(this)
    }
    private var disabled = false

    init {
        serverRef.addCallback(Game.TurnResult.UpdateCell::class) { result ->
            buttons[result.point].text = result.newValue.toString()
        }

        serverRef.addCallback(Game.TurnResult.Draw::class) {
            Alert(Alert.AlertType.NONE, "DRAW", ButtonType.CLOSE).showAndWait()
            sceneManager.disposeOne()
        }

        serverRef.addCallback(Game.TurnResult.Winner::class) {
            Alert(Alert.AlertType.NONE, "Someone Won", ButtonType.CLOSE).showAndWait()
            sceneManager.disposeOne()
        }

        for ((lineN, line) in buttons.lines.withIndex()) {
            for ((columnN, button) in line.withIndex()) {
                button.prefWidthProperty().bind(grid.widthProperty().divide(Game.SIZE))
                button.prefHeightProperty().bind(grid.heightProperty().divide(Game.SIZE))
                grid.add(button, lineN, columnN)
                button.setOnMouseClicked {
                    if (!disabled) {
                        serverRef.makeTurn(Point(lineN, columnN))
                    }
                }
            }
        }
    }

    override fun close() {
        serverRef.close()
        super.close()
    }
}
