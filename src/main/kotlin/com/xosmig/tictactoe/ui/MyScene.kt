package com.xosmig.tictactoe.ui

import javafx.scene.Parent
import javafx.scene.Scene
import java.io.Closeable

open class MyScene(parent: Parent, width: Double, height: Double): Scene(parent, width, height), Closeable {
    override fun close() {}
}
